<?php
//Load Init.php
require_once 'init.php';
//For get Users From json file
function getUsers($return_assoc = 0)
{
  $users  = json_decode(file_get_contents(USERS_DB), $return_assoc);
  return array_reverse((array) $users);
}
//Write Users To json file
function saveUser(stdClass $user): bool
{
  $uesrs  = getUsers(1);
  $uesrs[] = (array) $user;
  $uesrs_json = json_encode($uesrs);
  file_put_contents(USERS_DB, $uesrs_json);
  setcookie("register", "عضویت شما با موفقیت انجام شد.", time() + 2);
  redirect(BASE_URL);
}
//For Add Cookie
function addCookie($name, $msg, $time = 0)
{
  setcookie($name, $msg, time() + ($time), '/');
}

//For Delete Cookies
function deleteCookies($name)
{
  setcookie($name, "", time() - 3600, '/');
}
//For Redirect
function redirect($url)
{
  header('Location: ' . $url);
}
