<?php
include_once "init.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Sign Up & Login Form</title>
	<link rel="stylesheet" href="./style.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
	<style>
		body,
		.form-wrap form .button {
			direction: rtl;
			font-family: Tahoma;
		}

		.alert {
			text-align: center;
			padding: 5px;
			border-radius: 25px;
		}
	</style>
</head>

<body>

	<?php

	if (isset($_COOKIE["login"])) {
		?>
		<div class="alert form-wrap">
			<?php
			echo $_COOKIE["login"];
			?>
			<br><strong><a href="?logOut">خروج از سایت</a></strong>
		</div>
	<?php
	} else if (isset($_COOKIE["no-login"])) {
		?>
		<div class="alert form-wrap">
			<?php
			echo $_COOKIE["no-login"];
			?>
		</div>
	<?php
	} else if (isset($_COOKIE["register"])) {
		?>
		<div class="alert form-wrap">
			<?php
			echo $_COOKIE["register"];
			?>
		</div>
	<?php
	}

	?>

	</div>
	<!-- partial:index.partial.html -->
	<div class="form-wrap">
		<div class="tabs">
			<h3 class="signup-tab"><a href="#signup-tab-content">عضویت</a></h3>
			<h3 class="login-tab"><a class="active" href="#login-tab-content">ورود</a></h3>
		</div>
		<!--.tabs-->

		<div class="tabs-content">
			<div id="signup-tab-content">
				<form class="signup-form" action="init.php" method="post">
					<input type="email" name="email" class="input" id="user_email" autocomplete="off" placeholder="ایمیل">
					<input type="password" name="password" class="input" id="user_pass" autocomplete="off" placeholder="رمز">
					<input type="submit" class="button" name="signup" value="عضویت">
				</form>
				<!--.login-form-->

			</div>
			<!--.signup-tab-content-->

			<div id="login-tab-content" class="active">
				<form class="login-form" action="init.php" method="post">
					<input type="email" name="email" class="input" id="user_login" autocomplete="off" placeholder="ایمیل">
					<input type="password" name="password" class="input" id="user_pass" autocomplete="off" placeholder="رمز">
					<input type="submit" name="login" class="button" value="ورود">
				</form>
				<!--.login-form-->

			</div>
			<!--.login-tab-content-->
		</div>
		<!--.tabs-content-->
	</div>
	<!--.form-wrap-->
	<!-- partial -->
	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src="./script.js"></script>

</body>

</html>