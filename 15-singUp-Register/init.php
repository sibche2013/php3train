<?php
//Base Url of your Project
const BASE_URL = 'http://sign-login.test/';
//Base Path Of your Project
const BASE_PATH = 'C:\\lara\\www\\sign-login\\';
//Path Of Json Database
const USERS_DB = BASE_PATH . 'db\\users.json';
//Site Title
const SITE_TITLE = 'پیاده سازی عملیات ثبت نام، ورود و خروج با فایل json';
require_once 'functions.php';
//Check Form is sent with post method Or not ?
if ($_SERVER['REQUEST_METHOD'] == "POST") {
if(isset($_POST['signup'])){
  //Save Users On Json File
  unset($_POST['signup']);
  saveUser((object)$_POST);
}else if(isset($_POST['login'])){
  //Get Search Value
  $emailValue=$_POST['email'];
  $passValue=$_POST['password'];

  if (empty($emailValue) || empty($passValue)) {
    echo "لطفا ایمیل و رمز ورودتان را وارد نمایید.";
  }else{
   $users_DB = file_get_contents(USERS_DB);
	$users_Array = json_decode($users_DB, true);
 
    foreach ($users_Array as $key => $value) {

          if ((strpos($value['email'], $emailValue)!==false) && (strpos($value['password'], $passValue)!==false)) {
		 
		setcookie("login", "شما با موفقیت وارد سایت شدین.", time()+365*24*60);
		 redirect(BASE_URL);
		   }else{
			setcookie("no-login", "لطفا رمز و ایمیل خود را بدرستی وارد نمایید.", time()+2); 
		  redirect(BASE_URL);
		  }
    }
    
  }
  }
}else if ($_SERVER['REQUEST_METHOD'] == "GET") {
if(isset($_GET['logOut'])){
deleteCookies('login');
redirect(BASE_URL);
}
}
